# PHPetite (custom)

PHPetite (/p/h/pəˈtēt/) is a single file, static blog generated from PHP. Based off the very minimal and awesome <a target="_blank" href="https://github.com/cadars/portable-php">portable-php</a>.

This is **my** fork of the original <a target="_blank" href="https://github.com/bradleytaunt/phpetite">PHPetite</a> by Bradley Taunt. You should try *their* version instead of mine. I've released my version on an **AS IS** basis. I offer no guarantee that my version will work for you, and I am not prepared to provide free technical support of any kind.

## Key Features

- Entire blog is rendered in a single HTML file
- Inline, compressed CSS
- All images converted into base64 encoding
- Minimal requirements / no build tools heavier than `make`

## Core Principles

The basic idea behind PHPetite is to keep the concept and workflow as simple as possible. Therefore, this project will try it's best to avoid bloat and feature creep. More elaborate and feature-rich blogging platforms should be used if your needs are not met with PHPetite.

## Requirements

These are based on my using PHPetite on OpenBSD.

1. Install PHP with `doas pkg_add -iv php` and select 8.0.
2. Install rsync with `doas pkg_add -iv rsync` if you want to use `make install` to deploy your site.
3. Install python with `doas pkg_add -iv python3` if you want to use `make serve` to test your site locally.

### Original Requirements

1. `PHP 7.3` or higher
2. `GNU make`
3. If using Linux, you will require the following packages in order to convert your images to base64 encoding:
    - GNU make -> `sudo apt-get install build-essential`
    - PHP XML -> `sudo apt-get install php-xml`
    - PHP mbstring -> `sudo apt-get install php-mbstring`

That's really it!

## General Usage

You can find most basic explanations and details on working with the project at [phpetite.org](https://phpetite.org) if you prefer.

### Generating the Blog

Get [PHPetite](https://github.com/bradleytaunt/phpetite "PHPetite at Github") in order to convert a collection of Markdown files into a single HTML file with inline CSS.

1. Write posts in `/content`
2. (Optional) include any images under the `/content/img/` directory
3. From the command-line run:

```.bash
make
```

This will generate both the single file HTML page, along with a `feed/index.xml` file for the use of an optional RSS feed.

These two files are output into the `_site` directory.

---

### Structuring Blog Posts

Blog posts should be placed into the `/content` directory and be named based only on their post date and time. See an example here:

```.markdown
2048-01-01-23-58-00.md
```

You can use `make post` to create a new file with the current date and timestamp. This command will also use the editor specified in your shell's `$VISUAL` environment variable to edit the new file.

PHPetite will create a `target` by appending the page title inside the article to the file's date name. So a markdown file with the following content:

```.markdown
# Bladerunner Rocks

Bladerunner is amazing because blah blah blah...
```

will render out the `target` link as:

```.markdown
example.com/#2048-01-01-23-58-00-bladerunner-rocks
```

---

### Adding Custom Pages

To add your own custom pages, simply create a Markdown file under the `content/_pages` directory. PHPetite will take it from there!

#### Some Cavets

Any page you create will be automatically added to the `footer` navigation section. If you wish to hide individual pages from showing in the `footer`, do so via CSS:

```.css
footer a.slug-name-of-your-page {
    display: none;
}
```

If you want to remove the `footer` navigation altogether, add the following to your `style.css` file:

```.css
footer .footer-links {
    display: none;
}
```

### Testing

If you have `python3` installed, running `make serve` in a separate terminal (or tmux window) will serve your site at `http://localhost:8000` so that you can see how it looks in Firefox, Chromium, etc. When you're done, switch to that terminal and hit <kbd>CTRL+C</kbd> to stop the service.

If you have Seamonkey installed, file:// URLs still work in that browser.

Either way, run ```make``` to build whenever you like, then manually refresh your browser tab to see your changes. If you want a daemon to for changes and automatically refresh your browser, you're welcome to figure out how to implement that yourself and submit a patch.

---

## ISSUES

- Missing images will result in a failure to build on OpenBSD.
- PHPetite uses CSS3 selectors not implemented in console browsers like `lynx`.

## TO DO

- Proper accessibility audit
- Allow custom fonts to be set as Base64 strings
- ~~Set images as inline Base64 strings~~
- ~~Basic RSS feed~~
- ~~Automatically add new pages to footer nav~~
- ~~Compress inline CSS~~

## FAQ

### Why did you do this?

I wanted to make some adjustments to the original's stylesheet, and replace the `build.sh` script with a `makefile`. Also, I've been trying to make this work better with [Lynx](https://invisible-island.net/lynx/) for greater accessibility.

### Why doesn't this work in Lynx?

It's a little complicated. First, Lynx doesn't implement CSS3 selectors like `:target`. This isn't necessarily a problem, but PHPetite structures its HTML output backward:

1. Blog posts in reverse chronological order.
2. Pages
3. Home page with post listing (as #home)

The stylesheet targets #home by default and hides everything else in graphical browsers, but since Lynx doesn't support `:target` you see the oldest blog post first.

I'm not thrilled about this either, but I haven't figured out how to fix it without breaking things for graphical browsers yet.

### How do I use this on Windows?

#### simple answer

You don't.

#### complex answer

You'll need to set up some kind of GNU/Linux or BSD virtual machine and use this *there*. Instructions for that are beyond the scope of this document.

### Can you help me?

Perhaps, but I'd prefer not to. As I mentioned at the beginning of this document, my fork of PHPetite is available on an **AS IS** basis. I hope it works for you, but if it doesn't you're on your own.

### Why do I get "make: not found" when I try to build my site?

If you're on OpenBSD, FreeBSD, or NetBSD you should have `make` as part of the base system.

If you're on GNU/Linux, not all distributions provide developer tools like `GNU make` in the default installation. On Debian and Ubuntu, for example, you need to install them using a command like `sudo apt-get install build-essential`.

If you don't have access to `sudo`, please consult your systems administrator.


### Why do I get an error when I run "make post"?

Try running `echo $EDITOR`. If you get a blank line, you need to set this environment variable in your shell's config file. 

You can also set `$EDITOR` in the makefile itself. I was tempted to do this for you, but you might not want to use Emacs, or you might not have it installed where I do.
