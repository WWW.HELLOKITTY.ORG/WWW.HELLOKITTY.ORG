# Timestamped Post Test

This is a post whose filename is YYYY-MM-DD-HH-MM-SS instead of YYYY-MM-DD. This ensures that multiple posts within a given day are listed in the order in which they were created within the blog and Atom feed, something YYYY-MM-DD alone cannot do.
