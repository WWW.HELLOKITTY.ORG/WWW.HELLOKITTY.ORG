# Deployment Settings
SSH_OPTS=-o StrictHostKeyChecking=no
REMOTE_HOST="user@domain.tld"
REMOTE_PATH="/path/to/website"
NOW!=date +"%Y-%m-%d-%H-%M-%S"

.DEFAULT: all

all: commit build

commit:
	git add .
	git commit

build:
	mkdir -p _site/feed/
	php-8.0 _phpetite/phpetite.php > _site/index.html
	php-8.0 _phpetite/rss.php > _site/feed/index.xml

serve: build
	python3 -m http.server --directory _site/

install: all
	rsync --rsh="ssh ${SSH_OPTS}" \
		  --delete-delay \
		  -acvz \
		  _site/ ${REMOTE_HOST}:${REMOTE_PATH}

post:
	touch "content/${NOW}.md"
	${EDITOR} "content/${NOW}.md"

clean:
	rm -rf _site
